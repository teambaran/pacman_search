"""
In this file, you will implement generic search algorithms which are called by Pacman agents.
"""

import util
from util import Queue, PriorityQueue

# Called by search.depthFirstSearch.
def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first [p 85].

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm [Fig. 3.7].

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start: %s" % (str(problem.startingState())))
    print("Is the start a goal?: %s" % (problem.isGoal(problem.startingState())))
    print("Start's successors: %s" % (problem.successorStates(problem.startingState())))
    """
    #save initial position
    initial_node = problem.startingState()
    
    #check if initial state is goal if so don't need to do anything
    if problem.isGoal(initial_node):
        return None
        
    #adding the start position to the frontier
    frontier = [(initial_node, None, None)]
    
    #No nodes have been explored yet
    explored = []
    
    #A dictionary will be used to map moves to one another and will trace the path backwards from the goal node once it is found
    path_map = {}
    #adding the start position to the dictionary, it is mapped to None since it will represent the end of the path
    path_map[(initial_node, None, None)] = None
    
    
    #moves will be added to this list as they are traced from the goal
    path = []

    #while there are still things to explore...
    while frontier:
        #LIFO removal of elements from frontier
        node = frontier.pop()
        #Has the goal been found?
        if problem.isGoal(node[0]):
            #Hey! It has! Save the goal state in a temp var
            temp = node
            #While the beginning of the path has not been found...
            while path_map[temp] is not None:
                #Add the direction that got pacdude to the current location, to the path.
                path.append(temp[1])
                #backtrack to the previous move and repeat this process until the beginning has been found
                temp = path_map[temp]

            #since this path was found via backtracking, the ordering needs to be reversed
            path.reverse()
            #return the path
            return path
        
        #check if we have been here before, if so do nothing
        if node[0] not in explored:
            #otherwise mark our arrival
            explored.append(node[0])
            #find its successors
            #successors = [successor for successor in problem.successorStates(node[0])]
            successors = problem.successorStates(node[0])
            
            #add each of these successors to the frontier to be expanded later
            for successor in successors:
                frontier.append(successor)
                #prevents endless looping that occurs if multiple nodes have the same parent
                if successor not in path_map.keys():
                    path_map[successor] = node
                    
            
    #if a path can't be found return None
    return None
# Called by search.breadthFirstSearch.
def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first. [p 81]
    """

    # *** Your Code Here ***
    
    #save initial position
    initial_node = problem.startingState()
    #check if initial state is goal if so don't need to do anything
    if problem.isGoal(initial_node):
        return None
    #adding the start position to the frontier
    frontier = Queue()
    frontier.push((initial_node, None, None))
    explored = []
    #A dictionary will be used to map moves to one another and will trace the path backwards from the goal node once it is found
    path_map = {}
    #adding the start position to the dictionary, it is mapped to None since it will represent the end of the path
    path_map[(initial_node, None, None)] = None
    #moves will be added to this list as they are traced from the goal
    path = []

    #while there are still things to explore...
    while frontier:
        #FIFO removal of elements from frontier
        node = frontier.pop()
        #Has the goal been found?
        if problem.isGoal(node[0]):
            #Hey! It has! Save the goal state in a temp var
            temp = node
            #While the beginning of the path has not been found...
            while path_map[temp] is not None:
                #Add the direction that got pacdude to the current location to the path
                path.append(temp[1])
                #backtrack to the previous move and repeat this process until the beginning has been found
                temp = path_map[temp]

            #since this path was found via backtracking, the ordering needs to be reversed
            path.reverse()
            #return the path
            #print(path)
            return path
        #check if we have been here before, if so do nothing
        if node[0] not in explored:
            #otherwise mark our arrival
            explored.append(node[0])
            #find its successors
            successors = [successor for successor in problem.successorStates(node[0])]
            #add each of these successors to the frontier to be expanded later
            for successor in successors:
                frontier.push(successor)
                #prevents endless looping that occurs if multiple nodes have the same parent
                if successor not in path_map.keys():
                    path_map[successor] = node

    #if a path can't be found return None
    return None
# Called by search.uniformCostSearch.
def uniformCostSearch(problem):
    """
    Search the node of least total cost first.
    """
    
    #save initial position
    initial_node = problem.startingState()
    #check if initial state is goal if so don't need to do anything
    if problem.isGoal(initial_node):
        return None
    #adding the start position to the frontier
    frontier = PriorityQueue()
    frontier.push((initial_node, None, None),0)
    explored = []
    #A dictionary will be used to map moves to one another and will trace the path backwards from the goal node once it is found
    path_map = {}
    #adding the start position to the dictionary, it is mapped to None since it will represent the end of the path
    path_map[(initial_node, None, None)] = None
    #moves will be added to this list as they are traced from the goal
    path = []

    #while there are still things to explore...
    while not frontier.isEmpty():
        #PQ removal of elements from frontier

        node = frontier.pop()
        #Has the goal been found?
        if problem.isGoal(node[0]):
            #Hey! It has! Save the goal state in a temp var
            temp = node
            #While the beginning of the path has not been found...
            while path_map[temp] is not None:
                #Add the direction that got pacdude to the current location to the path
                path.append(temp[1])
                #backtrack to the previous move and repeat this process until the beginning has been found
                temp = path_map[temp]

            #since this path was found via backtracking, the ordering needs to be reversed
            path.reverse()
            #return the path
            return path
        #check if we have been here before, if so do nothing
        if node[0] not in explored:
            #otherwise mark our arrival
            explored.append(node[0])
            #find its successors
            successors = [successor for successor in problem.successorStates(node[0])]
            #add each of these successors to the frontier to be expanded later
            for successor in successors:
                frontier.push(successor, successor[2])
                #prevents endless looping that occurs if multiple nodes have the same parent
                if successor not in path_map.keys():
                    path_map[successor] = node

    #if a path can't be found return None
    return None

# Called by search.aStarSearch.
def aStarSearch(problem, heuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.
    """

    # *** Your Code Here ***
    """
    Search the node of least total cost first.
    """
    #save initial position
    initial_node = problem.startingState()
    #check if initial state is goal if so don't need to do anything
    if problem.isGoal(initial_node):
        return None
    #adding the start position to the frontier
    frontier = PriorityQueue()
    #(coordinates, direction, total_cost)
    frontier.push((initial_node, None, 0),0)
    explored = []
    #A dictionary will be used to map moves to one another and will trace the path backwards from the goal node once it is found
    path_map = {}
    #adding the start position to the dictionary, it is mapped to None since it will represent the end of the path
    path_map[(initial_node, None, 0)] = None
    #moves will be added to this list as they are traced from the goal
    path = []

    #while there are still things to explore...
    while not frontier.isEmpty():
        #PQ removal of elements from frontier

        node = frontier.pop()
        #total_cost = node[]
        #Has the goal been found?
        if problem.isGoal(node[0]):
            #Hey! It has! Save the goal state in a temp var
            temp = node
            #While the beginning of the path has not been found...
            while path_map[temp] is not None:
                #Add the direction that got pacdude to the current location to the path
                path.append(temp[1])
                #backtrack to the previous move and repeat this process until the beginning has been found
                temp = path_map[temp]

            #since this path was found via backtracking, the ordering needs to be reversed
            path.reverse()
            #return the path
            return path
        #check if we have been here before, if so do nothing
        if node[0] not in explored:
            
            #otherwise mark our arrival
            explored.append(node[0])
            #find its successors
            #add each of these successors to the frontier to be expanded later
            for successor in problem.successorStates(node[0]):
                cost = successor[-1] + heuristic(successor[0],problem)
                #push the coordinates, direction, and cost so far (excluding heuristic) with the cost+heuristic for the priority value
                frontier.push((successor[0], successor[1], node[-1]+successor[2]), node[-1]+ cost)
                #prevents endless looping that occurs if multiple nodes have the same parent
                if successor not in path_map.keys():
                    #print(successor)
                    path_map[(successor[0], successor[1], node[-1]+successor[2])] = node

    #if a path can't be found return None
    return None

